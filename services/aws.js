const aws = require('aws-sdk');
const AmazonS3URI = require('amazon-s3-uri');
require('dotenv').config();

aws.config.update({
    secretAccessKey :process.env.AWS_SECRET_KEY,
    accessKeyId : process.env.AWS_ACCESS_KEY_ID,
	region:process.env.AWS_REGION
});

const s3 = new aws.S3();

const options = {
	partSize:10*1024*1024,
	queueSize:1
};

async function upload(file,path){
	const params = {
		Bucket: process.env.AWS_BUCKET_NAME,
		Key: path,
		Body:file
	};
	let fileResp = null;

	await s3.upload(params,options)
	.promise()
	.then((res)=>{
		fileResp = res;
	})
    .catch(err => console.log("upload:", err))

	return fileResp;
}

function getSignedURL ( unsignedURL, hour = null ) {
    const { region, bucket, key } = AmazonS3URI( unsignedURL );
    if ( !bucket || !key || !region ) {
        return { error: "Invalid URI" };
    }
    const signedUrlExpireSeconds = hour === null ? ( 60*5 ) : ( hour*60*60 );
    return s3.getSignedUrl( 'getObject', {
        Bucket: bucket,
        Key: key,
        Expires: signedUrlExpireSeconds
    })
}

function getUnsignedUrl(path){
    const bucket = process.env.AWS_BUCKET_NAME
    const region = process.env.AWS_REGION
    return `https://${bucket}.s3.${region}.amazonaws.com/${path}`
}

module.exports = {
    upload,
    getSignedURL,
    getUnsignedUrl
};
