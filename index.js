'use strict';
require("dotenv").config();

const routes = require('./routes')
const {server, io} = require('./config/server')

const init = async () => {
    routes.forEach(route => {
        server.route(route);
    });

    io.on('connection', function(socket){
        console.log('new connection')
    })

    await server.start();
    console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});

init();