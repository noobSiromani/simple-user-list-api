const userController = require('../controllers/user')

module.exports = [
    {
        method: 'GET',
        path: '/users',
        handler: userController.fetchUsers
    },
    {
        method: 'POST',
        path: '/user',
        handler: userController.createUser
    },
    {
        method: 'POST',
        path: '/delete-users',
        handler: userController.deleteUser
    },
    {
        path:'/file-upload',
		method:'POST',
		options:{
			payload:{
				output:'stream',
				multipart:true,
                maxBytes: 20971520,
			},
			handler:userController.fileUplaod
		}
    },
    {
        path:'/file-url',
		method:'GET',
		handler:userController.fileUrl
    },
]