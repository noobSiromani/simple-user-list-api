const { Op } = require('sequelize');
const {io} = require('../config/server')

const {user: User} = require('../models')

const {upload, getSignedURL, getUnsignedUrl} = require('../services/aws')

const path = 'misc/abc.png'

exports.fetchUsers = async (req, res) => {
    try{
        let users = await User.findAll({
            order: [
                ['id', 'DESC']
            ]
        });
        return res.response({ 
            statusCode: 200,
            message: 'Users fetched successfully',
            data: {users}
        }).code(200);
    }catch(err){
        console.log(err)
        return res.response({ 
            statusCode: 200,
            message: err.message,
        }).code(200);
    }
}

exports.createUser = async (req, res) => {
    try{
        const existing_user = await User.findOne({
            where: {email: req.payload.email}
        })

        if(existing_user){
            throw new Error("Duplicate email ID")
        }

        const user = await User.create(req.payload)

        io.emit('new-user', {user})

        return res.response({ 
            statusCode: 200,
            message: 'User created successfully',
            data: {user}
        }).code(200);
    }catch(err){
        console.log(err)
        return res.response({ 
            statusCode: 200,
            message: err.message,
        }).code(200);
    }
}

exports.deleteUser = async (req, res) => {
    try{
        const {ids = []} = req.payload;
    
        await User.destroy({
            where: {id: {
                [Op.in]: ids
            }}
        })

        io.emit('users-deleted', {ids})
    
        return res.response({ 
            statusCode: 200,
            message: 'User deleted successfully',
        }).code(200);
    }catch(err){
        console.log(err)
        return res.response({ 
            statusCode: 200,
            message: err.message,
        }).code(200);
    }
}

exports.fileUplaod = async (req, res) => {
    try{
        const fileExt = req.payload.file.hapi.filename.split('.').pop()

        if(fileExt != 'png') throw new Error("As of now only png files are supported")

        await upload(req.payload.file, path)

        const url = getSignedURL(getUnsignedUrl(path))

        return res.response({ 
            statusCode: 200,
            message: 'File uploaded successfully',
            data: {url}
        }).code(200);
    }catch(err){
        console.log(err)
        return res.response({ 
            statusCode: 200,
            message: err.message,
        }).code(200);
    }
}

exports.fileUrl = async (req, res) => {
    try{
        const url = getSignedURL(getUnsignedUrl(path))

        return res.response({ 
            statusCode: 200,
            message: 'URL fetched successfully',
            data: {url}
        }).code(200);
    }catch(err){
        console.log(err)
        return res.response({ 
            statusCode: 200,
            message: err.message,
        }).code(200);
    }
}